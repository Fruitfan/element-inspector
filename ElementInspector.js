

/**
 * Returns the complete position data of a dom element, calculated from the top left of the page.
 * @param {HTMLElement} element The element to get the position data from
 * @returns {Object}
 */
function getPosition(element) {
    let xPosition = 0;
	let yPosition = 0;
	let height = element.offsetHeight;
    let width = element.offsetWidth;

    while(element) {
        xPosition += (element.offsetLeft + element.clientLeft);
        yPosition += (element.offsetTop + element.clientTop);
        element = element.offsetParent;
    }

    return { 
		left: xPosition, 
		right: xPosition + width,
		top: yPosition,
        bottom: yPosition + height,
        height: height,
        width: width
	};
}

export default {
    getPosition
};